package nl.utwente.di.celsiusToFahrenheit;

import java.util.HashMap;
import java.util.Map;

public class Calculation {

    public double fromCelToFah(String cel){
        return Integer.parseInt(cel) * 1.8 + 32;
    }
}
