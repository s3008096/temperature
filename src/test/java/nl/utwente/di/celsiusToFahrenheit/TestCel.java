package nl.utwente.di.celsiusToFahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * Tests the Quoter */
public class TestCel {

    @Test
    public void test5Cel() throws Exception {
        Calculation quoter = new Calculation();
        double price = quoter.fromCelToFah("5");
        Assertions.assertEquals(41.0, price, 0.0,
                "5 celsius to fahrenheit");
    }

}
